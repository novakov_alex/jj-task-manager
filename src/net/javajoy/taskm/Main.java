package net.javajoy.taskm;

import net.javajoy.taskm.dao.ListDao;
import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.db.DatabaseManager;
import net.javajoy.taskm.db.SingleConnectionManager;
import net.javajoy.taskm.parser.TaskParser;
import net.javajoy.taskm.sort.TaskPlanner;
import net.javajoy.taskm.swing.controller.TaskListController;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;
import org.apache.log4j.Logger;

import javax.swing.*;

/**
 * @author Alexey Novakov
 */
public class Main {
    private static Logger LOG = Logger.getLogger(Main.class);

    private final TaskDao taskDao;
    private final ListDao listDao;
    private TaskObserverFrame taskObserver;
    private TaskListTableModel tableModel;
    private TaskParser taskParser;
    private TaskPlanner taskPlanner;

    public static void main(String[] args) throws Exception {
        new Main();
    }

    public Main() throws Exception {
        LOG.info("Task Manager is started");
        new DatabaseManager(SingleConnectionManager.getDbConnectionOrNull()).checkSchema();
        taskDao = new TaskDao(SingleConnectionManager.getDbConnection());
        listDao = new ListDao(SingleConnectionManager.getDbConnection());

        tableModel = new TaskListTableModel();
        taskParser = new TaskParser();
        taskPlanner = new TaskPlanner();

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
                postGUIConstruction();
            }
        });
    }

    private void createAndShowGUI() {
        taskObserver = new TaskObserverFrame(taskPlanner, taskParser);
        taskObserver.setTaskListModel(tableModel);
    }

    private void postGUIConstruction() {
        try {
            new TaskListController(taskObserver, tableModel, taskDao, listDao, taskParser);
        } catch (Exception e) {
            LOG.error("Critical Error. Controller initialization was failed.", e);
        }
    }
}
