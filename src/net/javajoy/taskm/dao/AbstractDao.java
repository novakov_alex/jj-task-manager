package net.javajoy.taskm.dao;

import java.sql.Connection;

/**
 * @author Alexey Novakov
 */
public class AbstractDao {
    private Connection dbConnection;

    public AbstractDao(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public Connection getDbConnection() {
        return dbConnection;
    }
}
