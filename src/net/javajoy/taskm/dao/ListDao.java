package net.javajoy.taskm.dao;

import net.javajoy.taskm.entity.TaskList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Novakov
 */
public class ListDao extends AbstractDao {

    public ListDao(Connection dbConnection) {
        super(dbConnection);
    }


    public List<TaskList> getTaskList() throws SQLException {
        List<TaskList> list = new ArrayList<>();
        PreparedStatement statement = null;

        try {
            statement = getDbConnection().prepareStatement(
                    "select LIST_ID, NAME, CREATE_DATE from LIST");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            while (resultSet.next()) {
                TaskList taskList = new TaskList();
                taskList.setListId(resultSet.getInt(1));
                taskList.setName(resultSet.getString(2));
                taskList.setCreateDate(resultSet.getDate(3));
                list.add(taskList);
            }

        } finally {
            if (statement != null) {
                statement.close();
            }
        }

        return list;
    }
}
