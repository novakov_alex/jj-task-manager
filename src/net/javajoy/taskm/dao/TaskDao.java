package net.javajoy.taskm.dao;

import net.javajoy.taskm.entity.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Novakov
 */
public class TaskDao extends AbstractDao {
    public static final String TABLE_NAME = "TASK";

    public TaskDao(Connection dbConnection) {
        super(dbConnection);
    }

    public List<Task> getTasks(int listId) throws SQLException {
        List<Task> list = new ArrayList<>();
        PreparedStatement statement = null;

        try {
            statement = getDbConnection().prepareStatement(
                    "select T.TASK_ID, T.DESCRIPTION, T.LIST_ID, T.DUE_DATE " +
                            "from TM.TASK T " +
                            "  join TM.LIST L on T.LIST_ID = L.list_id " +
                            "WHERE " +
                            "T.COMPLETED = ? AND" +
                            " L.LIST_ID = ?");

            statement.setInt(1, 0);
            statement.setInt(2, listId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            while (resultSet.next()) {
                Task task = new Task();
                task.setTaskId(resultSet.getInt(1));
                task.setDescription(resultSet.getString(2));
                task.setListId(resultSet.getInt(3));
                task.setDueDate(resultSet.getDate(4));
                list.add(task);
            }

        } finally {
            if (statement != null) {
                statement.close();
            }
        }

        return list;
    }

    public void add(Task task) throws SQLException {
        PreparedStatement statement = null;

        try {
            statement = getDbConnection().prepareStatement(
                    "INSERT INTO TASK (description, list_id, due_date) VALUES (?,?,?)");
            statement.setString(1, task.getDescription());
            statement.setInt(2, task.getListId());
            statement.setDate(3, getDate(task));
            statement.execute();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    public void complete(Task task) throws SQLException {
        update(task);
    }

    public void postpone(Task task) throws SQLException {
        update(task);
    }

    public void change(Task task) throws SQLException {
        update(task);
    }

    private void update(Task task) throws SQLException {
        PreparedStatement statement = null;

        try {
            statement = getDbConnection().prepareStatement(
                    "UPDATE TASK SET " +
                            "COMPLETED = ?, DUE_DATE = ?, DESCRIPTION = ?, LIST_ID = ?" +
                            "WHERE TASK_ID = ?");
            statement.setInt(1, task.isCompleted() ? 1 : 0);
            statement.setDate(2, getDate(task));
            statement.setString(3, task.getDescription());
            statement.setInt(4, task.getListId());
            statement.setInt(5, task.getTaskId());
            statement.executeUpdate();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    private java.sql.Date getDate(Task task) {
        return task.getDueDate() != null ? new java.sql.Date(task.getDueDate().getTime()) : null;
    }

}
