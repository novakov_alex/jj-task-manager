package net.javajoy.taskm.db;

import net.javajoy.taskm.dao.TaskDao;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Scanner;

/**
 * @author Alexey Novakov
 */
public class DatabaseManager {
    private static Logger LOG = Logger.getLogger(DatabaseManager.class);

    public final static String DATABASE_NAME = "TaskManager";
    public static final String SCHEMA_NAME = "TM";

    public static final String SCHEMA_SCRIPT_NAME = "create_schema.sql";
    public static final String DROP_SCHEMA_SCRIPT_NAME = "drop_schema.sql";
    public static final String SQL_SCRIPTS_PATH = "sql";

    private Connection connection;

    public DatabaseManager(Connection connection) {
        this.connection = connection;
    }

    public void checkSchema() throws Exception {
        if (!isSchemaCreated()) {
            if (!createSchema()) {
                throw new Exception("Database could not created");
            }
        }
    }

    private boolean createSchema() throws IOException {
        boolean schemaCreated = false;

        try {
            executeSqlScript(getDDLScriptContent(DROP_SCHEMA_SCRIPT_NAME));
        } catch (Exception ignore) {
        }

        try {
            executeSqlScript(getDDLScriptContent(SCHEMA_SCRIPT_NAME));
            schemaCreated = true;
        } catch (SQLException ex) {
            LOG.error("Error on schema creation", ex);
        }

        return schemaCreated;
    }

    private void executeSqlScript(String sqlScript) throws SQLException {
        Scanner scanner = new Scanner(sqlScript).useDelimiter(";");
        Statement currentStatement = null;

        while (scanner.hasNext()) {
            String rawStatement = scanner.next();

            try {
                System.out.println("Raw statement \n" + rawStatement);
                currentStatement = connection.createStatement();
                currentStatement.execute(rawStatement);
            } finally {
                if (currentStatement != null) {
                    try {
                        currentStatement.close();
                    } catch (SQLException e) {
                        LOG.error("Error during the execution of script = " + sqlScript, e);
                    }
                }
                currentStatement = null;
            }
        }
    }

    private String getDDLScriptContent(String scriptName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(SQL_SCRIPTS_PATH + System.getProperty("file.separator") + scriptName)));
    }

    private boolean isSchemaCreated() throws Exception {
        if (connection == null) {
            createDatabase();
        }

        ResultSet rs;

        try {
            DatabaseMetaData dmd = connection.getMetaData();
            rs = dmd.getTables(DATABASE_NAME, SCHEMA_NAME, TaskDao.TABLE_NAME, null);
            return rs.next();
        } catch (Exception ex) {
            LOG.debug("Schema is not created", ex);
            return false;
        }
    }

    private void createDatabase() throws Exception {
        String userHomeDir = System.getProperty("user.home", ".");
        String systemDir = userHomeDir + "/.task_manager";
        System.setProperty("derby.system.home", systemDir);
        connection = SingleConnectionManager.getDbConnection(true);

        if (connection == null) {
            throw new Exception("Database could not be created");
        }
    }
}
