package net.javajoy.taskm.db;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Alexey Novakov
 */
public class SingleConnectionManager {
    private static Logger LOG = Logger.getLogger(SingleConnectionManager.class);

    private static Connection dbConnection;

    public static Connection getDbConnection() {
        return getDbConnection(false);
    }

    public static Connection getDbConnectionOrNull() {
        try {
            return getDbConnection(false);
        } catch (Exception ignore) {
            return null;
        }
    }

    public static Connection getDbConnection(boolean create) {
        if (dbConnection == null) {
            Properties props = new Properties();
            props.put("user", "tm");
            props.put("password", "tm");

            if (create) {
                props.put("create", "true");
            }

            try {
                dbConnection = DriverManager.getConnection(getJdbcUrl(), props);
            } catch (SQLException sqle) {
                LOG.error("Get database connection", sqle);
            }
        }
        LOG.trace("Getting db connection: " + dbConnection);
        return dbConnection;
    }

    public static String getJdbcUrl() {
        return "jdbc:derby:" + DatabaseManager.DATABASE_NAME;
    }
}
