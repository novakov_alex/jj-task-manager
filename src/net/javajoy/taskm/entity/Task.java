package net.javajoy.taskm.entity;

import java.util.Date;

/**
 * @author Alexey Novakov
 */
public class Task {
    private Date dueDate;
    private String description;
    private int listId;
    private boolean completed;
    private int taskId;
    private String tags;

    public Task() {

    }

    public Task(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (completed != task.completed) return false;
        if (listId != task.listId) return false;
        if (taskId != task.taskId) return false;
        if (!description.equals(task.description)) return false;
        if (!dueDate.equals(task.dueDate)) return false;
        if (tags != null ? !tags.equals(task.tags) : task.tags != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dueDate.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + listId;
        result = 31 * result + (completed ? 1 : 0);
        result = 31 * result + taskId;
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "dueDate=" + dueDate +
                ", description='" + description + '\'' +
                ", listId=" + listId +
                ", completed=" + completed +
                ", taskId=" + taskId +
                ", tags='" + tags + '\'' +
                '}';
    }
}
