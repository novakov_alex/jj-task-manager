package net.javajoy.taskm.entity;

import java.util.Date;

/**
 * @author Alexey Novakov
 */
public class TaskList {
    private int listId;
    private String name;
    private Date createDate;

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
