package net.javajoy.taskm.parser;

import net.javajoy.taskm.entity.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Alexey Novakov
 */
public class TaskParser {
    private static final String DATE_SEPARATOR = "^";

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public Task parseTask(String rawDescription) throws ParseException {
        String[] tokens = rawDescription.split("\\" + DATE_SEPARATOR);
        Task newTask = new Task(tokens[0].trim());

        if (tokens.length > 1) {
            Date dueDate;
            try {
                dueDate = parseDate(tokens[1].trim());
            } catch (Exception ex) {
                System.out.println("Cannot parse due date: " + ex.getMessage());
                throw ex;
            }

            newTask.setDueDate(dueDate);
        }

        return newTask;
    }

    public String convertToString(Task task) {
        String date = task.getDueDate() != null ? simpleDateFormat.format(task.getDueDate()) : "";
        return task.getDescription() + " " + DATE_SEPARATOR + date;
    }

    private Date parseDate(String token) throws ParseException {
        return simpleDateFormat.parse(token + "." + Calendar.getInstance().get(Calendar.YEAR));
    }
}
