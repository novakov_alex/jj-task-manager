package net.javajoy.taskm.sort;

import net.javajoy.taskm.entity.Task;

import java.util.Comparator;

/**
 * @author Alexey Novakov
 */
public class ComparatorFactory {
    private static Comparator<Task> dueDateComparator;

    public static Comparator<Task> getDueDateComparator() {
        if (dueDateComparator == null) {
            dueDateComparator = new Comparator<Task>() {
                @Override
                public int compare(Task left, Task right) {
                    if (left.getDueDate() == null && right.getDueDate() == null) {
                        return left.getDescription().compareTo(right.getDescription());
                    }

                    if (left.getDueDate() == null) {
                        return 1;
                    }

                    if (right.getDueDate() == null) {
                        return -1;
                    }

                    return left.getDueDate().compareTo(right.getDueDate());
                }
            };
        }

        return dueDateComparator;
    }
}
