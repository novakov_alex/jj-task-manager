package net.javajoy.taskm.sort;

import net.javajoy.taskm.entity.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Alexey Novakov
 */
public class TaskPlanner {
    private static SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");

    public boolean isCurrentTask(Task task) {
        return task.getDueDate() != null &&
                fmt.format(task.getDueDate()).equals(fmt.format(Calendar.getInstance().getTime()));
    }

    public boolean isOverdueTask(Task task) {
        return task.getDueDate() != null && task.getDueDate().compareTo(new Date()) == -1;
    }
}
