package net.javajoy.taskm.swing.controller;

import net.javajoy.taskm.dao.ListDao;
import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.parser.TaskParser;
import net.javajoy.taskm.swing.controller.action.*;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;

import javax.swing.*;

/**
 * @author Alexey Novakov
 */
public class TaskListController {
    public static final String INPUT_FIELD_HINT = "<< Add new task here ...";

    private final TaskListTableModel tableModel;
    private TaskObserverFrame form;
    private TaskDao taskDao;
    private ListDao taskListDao;
    private SelectListAction selectListAction;
    private TaskParser taskParser;

    public TaskListController(TaskObserverFrame taskObserver, TaskListTableModel tableModel,
                              TaskDao taskDao, ListDao listDao, TaskParser taskParser) throws Exception {
        this.form = taskObserver;
        this.tableModel = tableModel;
        this.taskDao = taskDao;
        this.taskListDao = listDao;
        this.taskParser = taskParser;
        init();
    }

    private void init() throws Exception {
        initTaskInput();
        initActionButtons();
        initTaskTable();
        loadDefaultTaskList();
        setActionOnListSelector();
    }

    private void initTaskTable() {
        form.getTaskTable().setDefaultEditor(Task.class,
                new EditTaskAction(taskDao, form.getTaskTable(), taskParser).createTableCellEditor());
    }

    private void initTaskInput() {
        NewTaskFocusAction newTaskFocusAction = new NewTaskFocusAction(form);
        form.getNewTaskInputField().addFocusListener(newTaskFocusAction);
        newTaskFocusAction.setHint(INPUT_FIELD_HINT);
        form.getNewTaskInputField().addActionListener(new NewTaskAction(form, tableModel, taskDao, taskParser));
    }

    private void initActionButtons() {
        form.getCompleteButton().addActionListener(new CompleteAction(form, taskDao, tableModel));
        form.getPostponeButton().addActionListener(new PostponeAction(form, taskDao, tableModel));
    }

    private void loadDefaultTaskList() throws Exception {
        selectListAction = new SelectListAction(form, tableModel, taskDao, taskListDao);
        selectListAction.loadDefaultList();
    }

    private void setActionOnListSelector() {
        for (JRadioButton button : form.getTaskButtonList()) {
            button.addActionListener(selectListAction);
        }
    }
}
