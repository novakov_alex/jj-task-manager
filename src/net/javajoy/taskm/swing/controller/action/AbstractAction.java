package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.sort.ComparatorFactory;
import net.javajoy.taskm.swing.model.TaskListTableModel;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Novakov
 */
public class AbstractAction {
    protected TaskListTableModel tableUIModel;

    public AbstractAction(TaskListTableModel tableUIModel) {
        this.tableUIModel = tableUIModel;
    }

    protected void sortUIModel() {
        sortUIModel(tableUIModel.getDataList());
    }

    protected void sortUIModel(List<Task> taskList) {
        Collections.sort(taskList, ComparatorFactory.getDueDateComparator());
        tableUIModel.setListSize(taskList.size());

        for (int i = 0; i < taskList.size(); i++) {
            tableUIModel.setValueAt(taskList.get(i), i);
        }

        tableUIModel.fireTableDataChanged();
    }
}
