package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;
import org.apache.log4j.Logger;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

/**
 * @author Alexey Novakov
 */
public class CompleteAction extends AbstractAction implements ActionListener {
    private static Logger LOG = Logger.getLogger(CompleteAction.class);

    private final TaskObserverFrame form;
    private TaskDao taskDao;

    public CompleteAction(TaskObserverFrame form, TaskDao taskDao, TaskListTableModel tableModel) {
        super(tableModel);
        this.form = form;
        this.taskDao = taskDao;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!form.getTaskTable().getSelectionModel().isSelectionEmpty()) {
            int[] selectedIndices = form.getTaskTable().getSelectedRows();
            stopTableEditing();

            try {
                for (int index : selectedIndices) {
                    Task task = tableUIModel.getValueAt(index);
                    task.setCompleted(true);
                    LOG.info("Task for completion: " + task);
                    taskDao.complete(task);
                    tableUIModel.removeRow(index);
                    LOG.debug("Task is completed: " + task);
                }
            } catch (Exception ex) {
                LOG.error("ERROR: tasks could not be updated, indices = " + Arrays.toString(selectedIndices), ex);
            }
        }
    }

    private void stopTableEditing() {
        if (form.getTaskTable().isEditing()) {
            form.getTaskTable().getCellEditor().stopCellEditing();
        }
    }
}
