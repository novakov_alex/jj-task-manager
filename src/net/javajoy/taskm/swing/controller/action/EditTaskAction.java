package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.parser.TaskParser;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * @author Alexey Novakov
 */
public class EditTaskAction {
    private static Logger LOG = Logger.getLogger(EditTaskAction.class);

    private final TaskParser taskParser;
    private TaskDao taskDao;
    private JTable taskTable;

    public EditTaskAction(TaskDao taskDao, JTable taskTable, TaskParser taskParser) {
        this.taskDao = taskDao;
        this.taskTable = taskTable;
        this.taskParser = taskParser;
    }

    private Task getUpdatedTaskState(String textTask) throws ParseException, SQLException {
        Task task = ((TaskListTableModel) taskTable.getModel()).getValueAt(taskTable.getEditingRow());
        Task parsedTask = taskParser.parseTask(textTask);
        LOG.debug("Edited task raw data: " + parsedTask);
        updateIfChanged(task, parsedTask);
        return task;
    }

    private void updateIfChanged(Task task, Task parsedTask) throws SQLException {
        task.setDescription(parsedTask.getDescription());
        task.setDueDate(parsedTask.getDueDate());

        boolean changed = false;
        if (!task.getDescription().equals(parsedTask.getDescription())) changed = true;
        else if (!task.getDueDate().equals(parsedTask.getDueDate())) changed = true;

        if (changed)
            taskDao.change(task);
    }

    public TableCellEditor createTableCellEditor() {
        return new Editor(this, taskParser);
    }

    public static class Editor extends AbstractCellEditor implements TableCellEditor {
        private final JTextField textField;
        private TaskParser taskParser;
        private EditTaskAction action;

        public Editor(EditTaskAction action, TaskParser taskParser) {
            this.action = action;
            this.taskParser = taskParser;
            this.textField = new JTextField();
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            textField.setText(taskParser.convertToString((Task) value));
            return textField;
        }

        @Override
        public Object getCellEditorValue() {
            try {
                return action.getUpdatedTaskState(textField.getText());
            } catch (ParseException e) {
                LOG.error("Error. Task is not parsed properly", e);
            } catch (SQLException e) {
                LOG.error("Error. Task is not updated properly", e);
            }

            cancelCellEditing();
            return null;
        }
    }
}
