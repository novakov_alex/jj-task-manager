package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.parser.TaskParser;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;
import org.apache.log4j.Logger;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Alexey Novakov
 */
public class NewTaskAction extends AbstractAction implements ActionListener {
    private static Logger LOG = Logger.getLogger(NewTaskAction.class);

    private final TaskObserverFrame form;
    private TaskParser parser;
    private TaskDao taskDao;

    public NewTaskAction(TaskObserverFrame form, TaskListTableModel tableModel, TaskDao taskDao, TaskParser taskParser) {
        super(tableModel);
        this.form = form;
        this.taskDao = taskDao;
        this.parser = taskParser;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        createTask();
        sortUIModel();
        releaseFocus();
    }

    private void releaseFocus() {
        form.getNewTaskInputField().transferFocus();
    }

    private void createTask() {
        String newTaskDescription = form.getNewTaskInputField().getText();

        if (null != newTaskDescription && newTaskDescription.trim().length() > 0) {
            try {
                Task task = parser.parseTask(newTaskDescription);
                LOG.debug("New Task to be added " + task);

                task.setListId(form.getSelectedTaskListId());
                taskDao.add(task);
                tableUIModel.addElement(task);
                return;
            } catch (Exception e) {
                LOG.error("task creation was failed.", e);
            }
        }

        LOG.error("Task description is not valid: \"" + newTaskDescription + "\"");
    }
}
