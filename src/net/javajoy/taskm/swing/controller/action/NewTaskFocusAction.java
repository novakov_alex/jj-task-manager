package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.swing.controller.TaskListController;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * @author Alexey Novakov
 */
public class NewTaskFocusAction implements FocusListener {

    private final TaskObserverFrame form;

    public NewTaskFocusAction(TaskObserverFrame form) {
        this.form = form;
    }

    @Override
    public void focusGained(FocusEvent e) {
        setHint("");
        form.setFocusedTaskInputFieldForeground();
    }

    @Override
    public void focusLost(FocusEvent e) {
        setHint(TaskListController.INPUT_FIELD_HINT);
        form.setReleasedTaskInputFieldForeground();
    }

    public void setHint(String hint) {
        form.getNewTaskInputField().setText(hint);
    }
}
