package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;
import net.javajoy.taskm.utils.TaskUtils;
import org.apache.log4j.Logger;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

/**
 * @author Alexey Novakov
 */
public class PostponeAction extends AbstractAction implements ActionListener {
    private static Logger LOG = Logger.getLogger(PostponeAction.class);

    private final TaskObserverFrame form;
    private TaskDao taskDao;

    public PostponeAction(TaskObserverFrame form, TaskDao taskDao, TaskListTableModel tableModel) {
        super(tableModel);
        this.form = form;
        this.taskDao = taskDao;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int[] selectedIndices = form.getTaskTable().getSelectedRows();

        if (!form.getTaskTable().getSelectionModel().isSelectionEmpty()) {
            try {
                for (int index : selectedIndices) {
                    Task task = tableUIModel.getValueAt(index);
                    ;
                    task.setDueDate(TaskUtils.calculateNewDate(task.getDueDate(), 1));
                    taskDao.postpone(task);
                }

                sortUIModel();
                form.getTaskTable().setRowSelectionInterval(selectedIndices[0], selectedIndices[selectedIndices.length - 1]);
            } catch (Exception ex) {
                LOG.error("Tasks could not be postponed with selected indices: " + Arrays.toString(selectedIndices), ex);
            }
        }
    }
}
