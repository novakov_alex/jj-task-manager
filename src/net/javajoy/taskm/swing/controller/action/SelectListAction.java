package net.javajoy.taskm.swing.controller.action;

import net.javajoy.taskm.dao.ListDao;
import net.javajoy.taskm.dao.TaskDao;
import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.entity.TaskList;
import net.javajoy.taskm.swing.model.TaskListTableModel;
import net.javajoy.taskm.swing.ui.TaskObserverFrame;
import org.apache.log4j.Logger;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author Alexey Novakov
 */
public class SelectListAction extends AbstractAction implements ActionListener {
    private static Logger LOG = Logger.getLogger(SelectListAction.class);

    private final TaskObserverFrame form;
    private TaskDao taskDao;
    private ListDao listDao;

    public SelectListAction(TaskObserverFrame form, TaskListTableModel tableModel, TaskDao taskDao, ListDao listDao) {
        super(tableModel);
        this.form = form;
        this.taskDao = taskDao;
        this.listDao = listDao;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int listId = Integer.parseInt(e.getActionCommand());

        try {
            LOG.debug("Load taskList by id: " + listId);
            loadTasksFromList(listId);
        } catch (Exception ex) {
            LOG.error("Tasks could not be loaded from list with id = " + listId, ex);
        }
    }

    public void loadDefaultList() throws Exception {
        List<TaskList> taskLists = listDao.getTaskList();

        if (!taskLists.isEmpty()) {
            loadTasksFromList(taskLists.get(0).getListId());
            form.initUITaskListGroup(taskLists);
        }
    }

    public void loadTasksFromList(int listId) throws Exception {
        List<Task> taskList = taskDao.getTasks(listId);
        sortUIModel(taskList);
    }
}
