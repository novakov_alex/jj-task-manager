package net.javajoy.taskm.swing.model;

import net.javajoy.taskm.entity.Task;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author Alexey Novakov
 */
public class TaskListTableModel extends DefaultTableModel {

    public TaskListTableModel() {
        setColumnCount(1);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return Task.class;
    }

    public void setValueAt(Task task, int rowIndex) {
        setValueAt(task, rowIndex, 0);
    }

    public void setListSize(int size) {
        getDataVector().clear();
        setRowCount(size);
    }

    public Task getValueAt(int rowIndex) {
        return (Task) getValueAt(rowIndex, 0);
    }

    public void addElement(Task task) {
        setRowCount(getRowCount() + 1);
        setValueAt(task, getRowCount() - 1, 0);
    }

    public List<Task> getDataList() {
        List<Task> taskList = new ArrayList<>();
        for (Object element : dataVector) {
            taskList.add((Task) ((Vector) element).get(0));
        }

        return taskList;
    }
}
