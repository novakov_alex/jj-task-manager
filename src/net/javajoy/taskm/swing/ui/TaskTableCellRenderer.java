package net.javajoy.taskm.swing.ui;

import net.javajoy.taskm.entity.Task;
import net.javajoy.taskm.sort.TaskPlanner;
import net.javajoy.taskm.utils.TaskUtils;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Map;

/**
 * @author Alexey Novakov
 */
public class TaskTableCellRenderer extends JPanel implements TableCellRenderer {
    public static final String CURRENT_TASK_TEXT = "Today";

    private TaskPlanner taskPlanner;
    private JLabel description = new JLabel();
    private JLabel dueDate = new JLabel();

    private Font regularTaskFont = new Font(Font.SERIF, Font.PLAIN, 14);
    private Font todayTaskFont = new Font(Font.SANS_SERIF, Font.BOLD, 14);

    public TaskTableCellRenderer(TaskPlanner taskPlanner) {
        this.taskPlanner = taskPlanner;
        initComponents();
    }

    private void initComponents() {
        description.setHorizontalTextPosition(SwingConstants.LEFT);
        dueDate.setHorizontalTextPosition(SwingConstants.RIGHT);

        setLayout(new GridBagLayout());
        setBackground(Color.white);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(3, 5, 3, 5);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.5;
        gbc.anchor = GridBagConstraints.WEST;
        add(description, gbc);

        gbc = new GridBagConstraints();
        gbc.insets = new Insets(0, 0, 0, 5);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.EAST;
        gbc.weightx = 0.5;
        add(dueDate, gbc);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Task task = (Task) value;

        description.setText(task.getDescription());
        dueDate.setText(TaskUtils.dueDateToString(task.getDueDate()));

        if (isSelected) {
            setBackground(new Color(121, 212, 212));
            setForeground(table.getForeground());
        } else {
            setBackground(Color.white);
            setForeground(table.getForeground());
        }

        styleImportantTasks(task);

        return this;
    }

    private void styleImportantTasks(Task task) {
        description.setFont(regularTaskFont);
        dueDate.setFont(regularTaskFont);

        if (taskPlanner.isCurrentTask(task)) {
            description.setFont(todayTaskFont);
            dueDate.setFont(todayTaskFont);
            dueDate.setText(CURRENT_TASK_TEXT);
        } else if (taskPlanner.isOverdueTask(task)) {
            setUnderlineFont(description);
            setUnderlineFont(dueDate);
        }
    }

    private void setUnderlineFont(JLabel label) {
        Map attributes = label.getFont().getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        label.setFont(label.getFont().deriveFont(attributes));
    }
}
