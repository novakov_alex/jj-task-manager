package net.javajoy.taskm.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Alexey Novakov
 */
public class TaskUtils {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public static String dueDateToString(Date dueDate) {
        return dueDate == null ? "" : dateFormat.format(dueDate);
    }

    public static Date calculateNewDate(Date originalDate, int daysOffset) {
        if (originalDate == null) return null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(originalDate);
        cal.add(Calendar.DAY_OF_MONTH, daysOffset);
        return cal.getTime();
    }

}
